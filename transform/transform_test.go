package transform

import (
	"JsonProcessor/clientreq"
	"reflect"
	"testing"
)

var (
	data1 = clientreq.Data{
		First:   "Andrew",
		Last:    "Martin",
		Email:   "andrew@gmail.com",
		Address: "nr.77",
		Created: "10.02.2019",
		Balance: "19",
	}
	data2 = clientreq.Data{
		First:   "Andrew",
		Last:    "Martin",
		Email:   "andrew@gmail.com",
		Address: "nr.77",
		Created: "10.02.2019",
		Balance: "19",
	}
	data3 = clientreq.Data{
		First:   "Andrew",
		Last:    "Martin",
		Email:   "andrew@gmail.com",
		Address: "nr.77",
		Created: "10.02.2019",
		Balance: "19",
	}
	data4 = clientreq.Data{
		First:   "Andrew",
		Last:    "Garfield",
		Email:   "andrew@gmail.com",
		Address: "nr.77",
		Created: "11.02.2019",
		Balance: "200",
	}
	data5 = clientreq.Data{
		First:   "Andrew",
		Last:    "Garfield",
		Email:   "andrew@gmail.com",
		Address: "nr.77",
		Created: "11.02.2019",
		Balance: "200",
	}
)

func TestRemoveDuplicates(t *testing.T) {
	var duplicateData []clientreq.Data
	duplicateData = append(duplicateData, data1, data2)
	got := removeDuplicateValues(duplicateData)
	want := map[clientreq.Data]struct{}{}
	want[data1] = struct{}{}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v, want %v", got, want)
	}
}

func TestGroupData(t *testing.T) {
	var records []clientreq.Data
	records = append(records, data3, data4)
	gData := GroupedData{
		Index:        "A",
		Records:      records,
		TotalRecords: 2,
	}
	var want []GroupedData
	want = append(want, gData)
	var unsortedData []clientreq.Data
	unsortedData = append(unsortedData, data3, data4, data5)
	got, _ := Group(unsortedData)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v, want %v", got, want)
	}
}

func TestEmptyData(t *testing.T) {
	var emptyData []clientreq.Data
	_, err := Group(emptyData)
	if err == nil {
		t.Errorf("Group() should return empty data error")
	}
}

func TestSortDataNoError(t *testing.T) {
	var unsortedData []clientreq.Data
	unsortedData = append(unsortedData, data1, data2)
	_, err := Group(unsortedData)
	if err != nil {
		t.Errorf("Group() error: %v", err)
	}
}
