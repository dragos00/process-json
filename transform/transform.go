package transform

import (
	"JsonProcessor/clientreq"
	"errors"
)

// GroupedData struct contains data grouped by index
type GroupedData struct {
	Index        string           `json:"index"`
	Records      []clientreq.Data `json:"records"`
	TotalRecords int              `json:"total_records"`
}

// Transform is used to group and filter data
type Transform interface {
	Group(data []clientreq.Data) ([]GroupedData, error)
}

func NewTransformer() Transform {
	return transformer{}
}

type transformer struct {
}

func (t transformer) Group(data []clientreq.Data) ([]GroupedData, error) {
	return Group(data)
}

// Group function take the data requested and group it by the first letter
// of Data struct. Removes the duplicates and returns []GroupedData.
// If Data is empty it returns and error
func Group(ungroupedData []clientreq.Data) ([]GroupedData, error) {
	if ungroupedData == nil {
		return nil, errors.New("empty data")
	}
	noDuplicates := removeDuplicateValues(ungroupedData)
	groupedMap := make(map[string][]clientreq.Data)
	for key := range noDuplicates {
		if key.First != "" {
			letter := key.First[0]
			groupedMap[string(letter)] = append(groupedMap[string(letter)], key)
		}
	}
	groupedData := convertMapToStruct(groupedMap)
	return groupedData, nil
}

func removeDuplicateValues(result []clientreq.Data) map[clientreq.Data]struct{} {
	noDuplicates := map[clientreq.Data]struct{}{}
	for _, value := range result {
		noDuplicates[value] = struct{}{}
	}
	return noDuplicates
}

func convertMapToStruct(mapData map[string][]clientreq.Data) []GroupedData {
	var groupedData []GroupedData
	var singleElement GroupedData
	for letter, value := range mapData {
		singleElement.Index = letter
		singleElement.Records = value
		singleElement.TotalRecords = len(value)
		groupedData = append(groupedData, singleElement)
	}
	return groupedData
}
