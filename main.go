package main

import (
	"JsonProcessor/application"
	"JsonProcessor/clientreq"
	"JsonProcessor/load"
	"JsonProcessor/transform"
	"log"
)

func main() {
	url := "https://randomapi.com/api/6de6abfedb24f889e0b5f675edc50deb?fmt=raw&sole"
	client := clientreq.New()
	e := clientreq.NewExtractor(client)
	t := transform.NewTransformer()
	fc := load.NewFileCreator()
	w := load.NewWriter(fc)
	err := application.Start(url, 300, e, t, w)
	if err != nil {
		log.Print(err)
		return
	}
}
