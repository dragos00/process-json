package clientreq

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

// Data struct contains data from an API call
type Data struct {
	First   string `json:"first"`
	Last    string `json:"last"`
	Email   string `json:"email"`
	Address string `json:"address"`
	Created string `json:"created"`
	Balance string `json:"balance"`
}

// Extract is used to get data from an API
type Extract interface {
	GetData(url string, numberOfLines int) ([]Data, error)
}

func NewExtractor(c HTTPClient) Extract {
	return extractor{c}
}

func (e extractor) GetData(url string, numberOfLines int) ([]Data, error) {
	return GetData(url, numberOfLines, e.client)
}

type extractor struct {
	client HTTPClient
}

// HTTPClient is used to make a http request
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type clientStruct struct {
	client http.Client
}

func (c clientStruct) Do(req *http.Request) (*http.Response, error) {
	return c.client.Do(req)
}

func New() HTTPClient {
	return clientStruct{}
}

// GetData takes an api url, numberOfLines(records) and HTTPClient interface
// and makes a http get request until the numberOfLines required and then
// unmarshal data in []Data. If not successful returns nil and error
func GetData(url string, numberOfLines int, client HTTPClient) ([]Data, error) {
	var result []Data
	var newResult []Data
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	for {
		response, err := client.Do(request)
		if err != nil {
			return nil, err
		}
		err = handleRequestCodes(response.StatusCode)
		if err != nil {
			return nil, err
		}
		newBody, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return nil, err
		}
		err = response.Body.Close()
		if err != nil {
			return nil, err
		}
		if err = json.Unmarshal(newBody, &newResult); err != nil { // Parse []byte to go struct pointer
			return nil, err
		}
		result = append(result, newResult...)
		if len(result) >= numberOfLines {
			break
		}
	}
	return result, err
}

func handleRequestCodes(statusCode int) error {
	if !(statusCode >= 200 && statusCode <= 299) {
		return errors.New("request not successful")
	}
	return nil
}
