package clientreq

import (
	"errors"
	"net/http"
	"testing"
)

type clientStub struct {
	err error
}

func (c clientStub) Do(req *http.Request) (*http.Response, error) {
	return nil, c.err
}

func TestGetDataClientError(t *testing.T) {
	c := clientStub{err: errors.New("request error")}
	var url string
	_, err := GetData(url, 150, c)
	if err == nil {
		t.Errorf("error expected")
	}
}

func TestHandleRequestCodeNilError(t *testing.T) {
	err := handleRequestCodes(200)
	if err != nil {
		t.Errorf("handleRequestCode error: %v", err)
	}
}

func TestHandleRequestCodeError(t *testing.T) {
	err := handleRequestCodes(100)
	if err == nil {
		t.Errorf("handleRequestCode should return error")
	}
}
