package application

import (
	"JsonProcessor/clientreq"
	"JsonProcessor/transform"
	"errors"
	"testing"
)

type stubExtractor struct {
	err error
}

func (e stubExtractor) GetData(url string, numberOfLines int) ([]clientreq.Data, error) {
	return nil, e.err
}

type stubTransformer struct {
	err error
}

func (t stubTransformer) Group(data []clientreq.Data) ([]transform.GroupedData, error) {
	return nil, t.err
}

type stubWriter struct {
	err error
}

func (w stubWriter) WriteJson(data []transform.GroupedData) error {
	return w.err
}

func TestStartExtractError(t *testing.T) {
	extractor := stubExtractor{errors.New("extract data error")}
	transformer := stubTransformer{}
	writer := stubWriter{}
	err := Start("random url", 100, extractor, transformer, writer)
	if err == nil {
		t.Errorf("Start() should return error")
	}
}

func TestStartTransformError(t *testing.T) {
	extractor := stubExtractor{}
	transformer := stubTransformer{errors.New("transform data error")}
	writer := stubWriter{}
	err := Start("random url", 100, extractor, transformer, writer)
	if err == nil {
		t.Errorf("Start() should return error")
	}
}
func TestStartWriteError(t *testing.T) {
	extractor := stubExtractor{}
	transformer := stubTransformer{}
	writer := stubWriter{errors.New("write error")}
	err := Start("random url", 100, extractor, transformer, writer)
	if err == nil {
		t.Errorf("Start() should return error")
	}
}

func TestStartNoErrorError(t *testing.T) {
	extractor := stubExtractor{}
	transformer := stubTransformer{}
	writer := stubWriter{}
	err := Start("random url", 100, extractor, transformer, writer)
	if err != nil {
		t.Errorf("Start() error: %v", err)
	}
}
