package application

import (
	"JsonProcessor/clientreq"
	"JsonProcessor/transform"
	"log"
)

// Extract is used to get data from an API
type Extract interface {
	GetData(url string, numberOfLines int) ([]clientreq.Data, error)
}

// Transform is used to group and filter data
type Transform interface {
	Group(data []clientreq.Data) ([]transform.GroupedData, error)
}

// Write is used to write json data in a file
type Write interface {
	WriteJson(data []transform.GroupedData) error
}

// Start combines the logic of the application. It takes an url, numberOfLines
// and 3 interfaces and executes every step: extract, transform, load.
// If successful the error is nil and the application should work.
// If not successful returns an error related to one of the steps
func Start(url string, numberOfLines int, extract Extract, transform Transform, write Write) error {
	data, err := extract.GetData(url, numberOfLines)
	if err != nil {
		log.Print(err)
		return err
	}
	transformedData, err := transform.Group(data)
	if err != nil {
		log.Print(err)
		return err
	}
	err = write.WriteJson(transformedData)
	if err != nil {
		log.Print(err)
		return err
	}
	return nil
}
