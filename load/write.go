package load

import (
	"JsonProcessor/transform"
	"encoding/json"
	"io"
	"os"
)

// Write is used to write json data in a file
type Write interface {
	WriteJson(data []transform.GroupedData) error
}

func NewWriter(fc FileCreator) Write {
	return writer{fc}
}

type writer struct {
	fc FileCreator
}

func (w writer) WriteJson(data []transform.GroupedData) error {
	return WriteJson(data, w.fc)
}

// FileCreator is used to create a file
type FileCreator interface {
	Create(name string) (*os.File, error)
}

type creator struct {
}

func (c creator) Create(name string) (*os.File, error) {
	return os.Create(name)
}

func NewFileCreator() FileCreator {
	return creator{}
}

// WriteJson creates a file and then write []GroupedData to it
// If not successful returns an error related to file creation
// writing data or file.Close()
func WriteJson(data []transform.GroupedData, fc FileCreator) error {
	for _, group := range data {
		fileName := createFileName(group.Index)
		outputFile, err := fc.Create(fileName)
		if err != nil {
			return err
		}
		err = writeData(outputFile, group)
		if err != nil {
			return err
		}
		err = outputFile.Close()
		if err != nil {
			return err
		}
	}
	return nil
}

func writeData(writer io.Writer, data transform.GroupedData) error {
	formattedData, err := format(data)
	if err != nil {
		return err
	}
	_, err = writer.Write(formattedData)
	if err != nil {
		return err
	}
	return nil
}

func createFileName(letter string) string {
	fileName := letter
	fileName = fileName + ".json"
	return fileName
}

func format(i interface{}) ([]byte, error) {
	s, err := json.MarshalIndent(i, "", "\t")
	if err != nil {
		return nil, err
	}
	return s, nil
}
