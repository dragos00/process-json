package load

import (
	"JsonProcessor/transform"
	"bytes"
	"errors"
	"os"
	"testing"
)

type stubFileCreator struct {
	err error
}

func (f stubFileCreator) Create(name string) (*os.File, error) {
	return nil, f.err
}

type stubWriter struct {
	err error
}

func (w stubWriter) Write(p []byte) (n int, err error) {
	return 0, w.err
}

func TestWriteJsonError(t *testing.T) {
	var testData []transform.GroupedData
	stubData1 := transform.GroupedData{
		Index:        "A",
		Records:      nil,
		TotalRecords: 0,
	}
	testData = append(testData, stubData1)
	fileCreator := stubFileCreator{}
	fileCreator.err = errors.New("error while creating file")
	err := WriteJson(testData, fileCreator)
	if err == nil {
		t.Errorf("WriteJson() is not returning error")
	}
}

func TestWriteData(t *testing.T) {
	file := &bytes.Buffer{}
	var testData transform.GroupedData
	err := writeData(file, testData)
	if err != nil {
		t.Errorf("writeData() error = %v", err)
	}
}

func TestWriteDataError(t *testing.T) {
	writer := stubWriter{}
	writer.err = errors.New("writing failed")
	var testData transform.GroupedData
	err := writeData(writer, testData)
	if err == nil {
		t.Errorf("writeData() is not returning error")
	}
}

func TestWriteDataNoError(t *testing.T) {
	writer := stubWriter{}
	var testData transform.GroupedData
	err := writeData(writer, testData)
	if err != nil {
		t.Errorf("writeData() error = %v", err)
	}
}
